**NOTAMS 5G MONITORING ROUTINE FOR 5G C-BAND NOTAMS (AIRPORT NOTAMS)**

This routine was designed to monitor and send email reports concerning Airport NOTAMS related to "5G C-BAND" issues in US airports. The categories being monitored include "AD", "IAP", "SPECI" and "(RNP)" NOTAMS.

*We reccommend liaising with LIS-OpsTechnical@netjets.com for more information. Program developed by AFAISCA*

---

## Code Architechture

Here is a description of the key functions and aspects of the monitoring routine:

1. Routine fetches ALL NOTAMS from NetJets https://servicesprod.netjets.com/notam/v1/notams? API for a list of airports present in the "Icaos starting with K.csv" file present in the Support Documents;
2. The program parses and decodes the QCODE using support documents for QCODE decoding;
3. Program checks and filters NOTAMS by category and if they are related to 5G C-Band;
4. If not present in the Database, they will be tagged as "NEW". On the contrary if any STATUS=ACTIVE NOTAM present in the DB is NOT found in the downloaded NOTAMS, then it will be set as STATUS=DELETED and will be shown to the users as a deleted NOTAMM;
5. Email is sent to users with either NEW / DELETED Notams, as well as some statistics and counts of totals. A secondary "ALL ACTIVE" 5G C-BAND NOTAMS .xlsx report is also attached to this email.
6. Language of the email is html. 

---

