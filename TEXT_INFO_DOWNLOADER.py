# -*- coding: utf-8 -*-
"""
Created on Tue Mar 23 15:31:54 2021

@author: afaisca
"""
from datetime import datetime
from os import path
import pandas as pd
import requests
import os
import json

def text_info_NOTAM_NETJETS_API(TEXT_INFO, ICAO_LIST):
    sorterIndex = dict(zip(ICAO_LIST, range(len(ICAO_LIST))))
    ICAO_LIST = ','.join(ICAO_LIST)
    ERRORS = ''
    
    with open("drive_paths.json", "r") as f:
        drives = json.load(f)
        
        
    main_path = os.path.expanduser(drives['local_drives']['main_path'])
    QCODES_main = pd.read_csv(main_path + 'Support Documents\\QCODE.csv')
    QCODES_FIRST = pd.read_csv(main_path + 'Support Documents\\QCODE_FIRST.csv')
    QCODES_SECOND = pd.read_csv(main_path + 'Support Documents\\QCODE_SECOND.csv')
    
    now = datetime.now()
    dt_string = now.strftime("%Y%m%d %H:%M")
    
    #Welcome message
    # print('\n\nWelcome to the NetJets Wx Downloader for Electronic Flight Folder!')
    # print('Current file date: ' + dt_string)
    
    if TEXT_INFO == 'TAF + METAR + NOTAMs' or TEXT_INFO == 'NOTAMs':
        url = "https://servicesprod.netjets.com/notam/v1/notams?airport=" + ICAO_LIST + "&appagent=DpatchSuppSysUser"
    
        payload={}
        headers = {
          'Cookie': 'JSESSIONID=80F6E060B2E42F8C35D86087FB2C3806.notam-svc-prod'
        }
        
        response = requests.request("GET", url, headers=headers, data=payload)
        if response == []:
            ERRORS = 'NO NOTAM DATA COULD BE RETRIEVED AT THIS TIME :('
            fresh_NOTAMS = []
            return fresh_NOTAMS, ERRORS
             
        else:
            response = response.json()
            response = response['notams']
            
            downloaded_content = pd.DataFrame(columns=['FIR','ICAO','NOTAM','QCODE','QCODES_UNDECODED','FROM','TO','SCHEDULE','TEXT','ITEMF','ITEMG','NOTAM_LONG_NUMBER','VERSION','SOURCE','NOTAM_TYPE','RUNWAYS','SCORE','FIRST_LETTER_SCORE','ONE_TWO_SCORE','THREE_FOUR_SCORE'])
                    
            for index, each in enumerate(response):
                FIR = ''
                NOTAM = each['srcNumber']
                QCODES = []
                QCODES_UNDECODED = []
                QCODE = each['qcodes']
                for i, actual in enumerate(QCODE):
                    QCODE = actual['qcode']
                    QCODE_UNDECODED = actual['qcode']
                    FIRST_LETTER = QCODE[:1]
                    ONE_TWO = QCODE[:2]
                    THREE_FOUR = QCODE[2:]
                    
                    try:
                        FIRST_LETTER = QCODES_main[QCODES_main['LETTER'].str.contains(FIRST_LETTER)]
                        FIRST_LETTER = FIRST_LETTER.reset_index(drop=True)
                        FIRST_LETTER_SCORE = FIRST_LETTER['SCORE'][0]
                        FIRST_LETTER = FIRST_LETTER['TEXT'][0]
                        
                        ONE_TWO = QCODES_FIRST[QCODES_FIRST['LETTER'].str.contains(ONE_TWO)]
                        ONE_TWO = ONE_TWO.reset_index(drop=True)
                        ONE_TWO_SCORE = ONE_TWO['SCORE'][0]
                        ONE_TWO = ONE_TWO['TEXT'][0]
                        
                        THREE_FOUR = QCODES_SECOND[QCODES_SECOND['LETTER'].str.contains(THREE_FOUR)]
                        THREE_FOUR = THREE_FOUR.reset_index(drop=True)
                        THREE_FOUR_SCORE = THREE_FOUR['SCORE'][0]
                        THREE_FOUR = THREE_FOUR['TEXT'][0]
                        
                        QCODE = FIRST_LETTER.upper() + ':' + ONE_TWO + ' ' + THREE_FOUR
                        SCORE = FIRST_LETTER_SCORE + ONE_TWO_SCORE + THREE_FOUR_SCORE
                        
                        #print(QCODE)
                    except:
                        #print(QCODE)
                        pass
                    
                    QCODES.append(QCODE)
                    QCODES_UNDECODED.append(QCODE_UNDECODED)
                    
                QCODES = ','.join(QCODES)
                QCODES_UNDECODED = ','.join(QCODES_UNDECODED)
                FROM = each['effectiveDate']
                TO = each['expirationDate']
                FORMATTED_HEADER = each['formatted'].split(' ')
                FORMATTED_HEADER = [i for i in FORMATTED_HEADER if i]
                # if TO == "3000-12-31T23:59:59Z":
                #     TO = 'PERM'
                    
                if len(FROM) == 20 and FROM !="3000-12-31T23:59:59Z":
                    FROM = FROM.replace('T',' ')
                    FROM = FROM[:-4] + 'Z'
                    FROM = FROM.replace(":","")
                else:
                    FROM = FORMATTED_HEADER[5]
                    print(FROM)
                    
                if len(TO) == 20 and TO !="3000-12-31T23:59:59Z":
                    TO = TO.replace('T',' ')
                    TO = TO[:-4] + 'Z'
                    TO = TO.replace(":","")
                else:
                    TO = FORMATTED_HEADER[6]
                    #print(TO)
                    
                    
                try:
                    SCHEDULE = each['schedule'].replace("\n","<br/>")
                    if SCHEDULE[:5] == '<br/>':
                        SCHEDULE = SCHEDULE[5:]
                    if SCHEDULE[-5:] == '<br/>':
                        SCHEDULE = SCHEDULE[:-5]
                except:
                    SCHEDULE = ''
                TEXT = each['content']
                try:
                    ITEMF_ITEMG = (each['formatted'].replace('\n','')).replace(' ','').split((TEXT.replace('\n','')).replace(' ',''))
                    ITEMF_ITEMG = ITEMF_ITEMG[1]
                except:
                    if QCODE == 'MRHH' or NOTAM[0] == 'S':
                        ITEMF_ITEMG = ''
                
                TEXT = each['content'].replace("\n","<br/>")
                if TEXT[:5] == '<br/>':
                    TEXT = TEXT[5:]
                if TEXT[-5:] == '<br/>':
                    TEXT = TEXT[:-5]
                
                if ITEMF_ITEMG != '':
                    try:
                        ITEMF_ITEMG = ITEMF_ITEMG.replace('AMSL','AMSL ')
                        ITEMF_ITEMG = ITEMF_ITEMG.replace('SFC','SFC ')
                        ITEMF_ITEMG = ITEMF_ITEMG.replace('GND','GND ')
                        
                        try:
                            ITEMF = ITEMF_ITEMG.split(' ')[0].replace('FT',' FT ')
                            ITEMG = ITEMF_ITEMG.split(' ')[1].replace('FT',' FT ')
                        except:
                            pass
                        
                        ITEMF = ITEMF.replace('AGL',' AGL ')
                        ITEMG = ITEMG.replace('AGL',' AGL ')
                        
                        if 'FL' in ITEMF_ITEMG:
                            if 'SFC' in ITEMF_ITEMG:
                                ITEMF = ITEMF_ITEMG.split(' ')[0]
                                ITEMG = ITEMF_ITEMG.split(' ')[1]
                            else:
                                ITEMF = 'FL' + ITEMF_ITEMG.split('FL')[1]
                                ITEMG = 'FL' + ITEMF_ITEMG.split('FL')[2]
                            
                        elif 'FT' not in ITEMF_ITEMG:
                            ITEMF = ITEMF_ITEMG.split(' ')[0].replace('M',' M ')
                            ITEMG = ITEMF_ITEMG.split(' ')[1].replace('M',' M ')
                            
                        ITEMF = ITEMF.replace('  ',' ')
                        ITEMG = ITEMG.replace('  ',' ')
                    except:
                        ITEMF = ''
                        ITEMG = ''
                        
                else:
                    ITEMF = ''
                    ITEMG = ''
                NOTAM_LONG_NUMBER = each['jeppNumber']
                VERSION = each['jeppVersion']
                ICAO = each['airportId']
                SOURCE = each['source']
                NOTAM_TYPE = each['notamType']
                try:
                    RWY_AFFECTED = ','.join(each['runways'])
                except:
                    RWY_AFFECTED = 'NOT APPLICABLE'
                    
                
                downloaded_content_single = pd.DataFrame(columns=['FIR','ICAO','NOTAM','QCODE','QCODES_UNDECODED','FROM','TO','SCHEDULE','TEXT','ITEMF','ITEMG','NOTAM_LONG_NUMBER','VERSION','SOURCE','NOTAM_TYPE','RUNWAYS','SCORE','FIRST_LETTER_SCORE','ONE_TWO_SCORE','THREE_FOUR_SCORE'])
                    
                NOTAM_ROW = [FIR,ICAO,NOTAM,QCODES,QCODES_UNDECODED,FROM,TO,SCHEDULE,TEXT, ITEMF,ITEMG,NOTAM_LONG_NUMBER,VERSION,SOURCE,NOTAM_TYPE,RWY_AFFECTED,SCORE,FIRST_LETTER_SCORE,ONE_TWO_SCORE,THREE_FOUR_SCORE]
                downloaded_content_len = len(downloaded_content_single)
                #Keep adding the "current" downloaded_content_single rows to the general downloaded_content dataframe, each line to be added at the bottom of the existing dataframe.
                downloaded_content_single.loc[downloaded_content_len] = NOTAM_ROW
                #Append the single row to the bottom of the downloaded_content dataframe.
                downloaded_content = pd.concat([downloaded_content,downloaded_content_single])

            ICAOS_DOWNLOADED = downloaded_content['ICAO'].to_list()
            ICAOS_DOWNLOADED = list(dict.fromkeys(ICAOS_DOWNLOADED))

            for n, ICAO_code in enumerate(ICAO_LIST.split(',')):
                if ICAO_code not in ICAOS_DOWNLOADED:
                    print('\nWARNING: ICAO NOT AVAILABLE ON JEPPESEN...')
                    print('ERROR MESSAGE: ' + ICAO_code + ' NOTAMs COULD NOT BE DOWNLOADED AT THIS TIME!')
                    ERRORS = ERRORS + '\nWARNING: ICAO NOT AVAILABLE ON JETPLAN...\nICAO CODE: ' + ICAO_code
             
        fresh_NOTAMS = downloaded_content.sort_values(by=['ICAO', 'SCORE']).reset_index(drop=True)
        fresh_NOTAMS['Rank'] = fresh_NOTAMS['ICAO'].map(sorterIndex)
        fresh_NOTAMS.sort_values(['Rank', 'SCORE', 'QCODE'],ascending = [True, True, True], inplace = True)
        fresh_NOTAMS = fresh_NOTAMS.reset_index(drop = True)
                
    if TEXT_INFO == 'TAF + METAR + NOTAMs' or TEXT_INFO == 'NOTAMs':
        return fresh_NOTAMS, ERRORS
    else:
        fresh_NOTAMS = []
        return fresh_NOTAMS, ERRORS
                        
def text_info_METAR_NETJETS_API(TEXT_INFO, ICAO_LIST):
    ICAO_LIST = ','.join(ICAO_LIST)
    ERRORS = ''
    
    url = "https://servicesprod.netjets.com/weatherForecast/v2/airportWxs/" + ICAO_LIST + ".json?appagent=DpatchSuppSysUser"

    payload={}
    headers = {
      'Cookie': 'JSESSIONID=142EF2894B6BB46FD885DABFDCB70C53.weather-svc-prod; JSESSIONID=EFDDA99106086621BB357497F52210A9.weather-svc-prod'
    }
    
    response = requests.request("GET", url, headers=headers, data=payload)
    response = response.json()
    
    downloaded_content_METAR_TAF = pd.DataFrame(columns=['ICAO','METAR/TAF','TEXT'])
    
    if TEXT_INFO == 'TAF + METAR + NOTAMs' or TEXT_INFO == 'TAF + METAR':
            
        for index, each in enumerate(response):
            
            ICAO = each
            metar_info = response[each]['metars']
            if metar_info == []:
                downloaded_content_METAR_TAF_single = pd.DataFrame(columns=['ICAO','METAR/TAF','TEXT'])
                METAR_TAF_ROW = [ICAO,'METAR','**METAR INFO NOT AVAILABLE FOR THIS ICAO**']
                downloaded_content_len_METAR_TAF = len(downloaded_content_METAR_TAF_single)
                #Keep adding the "current" downloaded_content_single rows to the general downloaded_content dataframe, each line to be added at the bottom of the existing dataframe.
                downloaded_content_METAR_TAF_single.loc[downloaded_content_len_METAR_TAF] = METAR_TAF_ROW
                #Append the single row to the bottom of the downloaded_content dataframe.
                downloaded_content_METAR_TAF = pd.concat([downloaded_content_METAR_TAF,downloaded_content_METAR_TAF_single])
                
            for i, actual in enumerate(metar_info):
                TYPE = 'METAR'
                if metar_info[i][:5] == 'SPECI':
                    TYPE = 'SPECI METAR'
                
                if 'METAR' not in actual:
                    actual = 'METAR' + ' ' + metar_info[i] 
                    
                    
                downloaded_content_METAR_TAF_single = pd.DataFrame(columns=['ICAO','METAR/TAF','TEXT'])
                METAR_TAF_ROW = [ICAO,TYPE,actual]
                downloaded_content_len_METAR_TAF = len(downloaded_content_METAR_TAF_single)
                #Keep adding the "current" downloaded_content_single rows to the general downloaded_content dataframe, each line to be added at the bottom of the existing dataframe.
                downloaded_content_METAR_TAF_single.loc[downloaded_content_len_METAR_TAF] = METAR_TAF_ROW
                #Append the single row to the bottom of the downloaded_content dataframe.
                downloaded_content_METAR_TAF = pd.concat([downloaded_content_METAR_TAF,downloaded_content_METAR_TAF_single])
                
            taf_info = response[each]['taf']
            if str(type(taf_info)) == "<class 'NoneType'>":
                taf_info = '**TAF INFO NOT AVAILABLE FOR THIS ICAO**'
                TYPE = 'TAF'
            else:
                if 'TAF' in taf_info or taf_info[17] == '/':
                    
                    if 'NIL=' in taf_info:
                        TYPE = 'TAF'
                    elif taf_info[17] == '/':
                        taf_info = 'TAF ' + taf_info
                        TYPE = 'TAF'
                    elif taf_info[4:7] == 'AMD':
                        TYPE = 'TAF AMD'
                    elif taf_info[4:7] == 'COR':
                        TYPE = 'TAF COR'
                    else:
                        TYPE = 'TAF'
                        
                    if 'TAF' not in taf_info:
                        taf_info = TYPE + ' ' + taf_info
                            
                            
            downloaded_content_METAR_TAF_single = pd.DataFrame(columns=['ICAO','METAR/TAF','TEXT'])
            METAR_TAF_ROW = [ICAO,TYPE,taf_info]
            downloaded_content_len_METAR_TAF = len(downloaded_content_METAR_TAF_single)
            #Keep adding the "current" downloaded_content_single rows to the general downloaded_content dataframe, each line to be added at the bottom of the existing dataframe.
            downloaded_content_METAR_TAF_single.loc[downloaded_content_len_METAR_TAF] = METAR_TAF_ROW
            #Append the single row to the bottom of the downloaded_content dataframe.
            downloaded_content_METAR_TAF = pd.concat([downloaded_content_METAR_TAF,downloaded_content_METAR_TAF_single])
        
        fresh_TAF_METAR = downloaded_content_METAR_TAF.reset_index(drop=True)
        
    if TEXT_INFO == 'TAF + METAR + NOTAMs' or TEXT_INFO == 'TAF + METAR':
        return fresh_TAF_METAR, ERRORS
    else:
        fresh_TAF_METAR = []
        return fresh_TAF_METAR, ERRORS


