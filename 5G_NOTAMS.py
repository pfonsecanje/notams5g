# -*- coding: utf-8 -*-
"""
Created on Wed Jan  5 12:08:39 2022

@author: afaisca
"""
import smtplib
import sqlite3
from datetime import datetime
import pandas as pd
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email import encoders
from email.mime.base import MIMEBase
from TEXT_INFO_DOWNLOADER import text_info_NOTAM_NETJETS_API

NOTAMS_NEW = []
NOTAMS_CHANGED = []

def PREPARE_EXCEL(database_path,ICAO_LIST):
    ICAO_LIST = pd.DataFrame(ICAO_LIST,columns =['ICAO CODES'])
    
    ID, DB_ACCESS = read_DB(database_path)
    IAP_NOTAMS_ALL = DB_ACCESS[DB_ACCESS['STATUS'] == 'ACTIVE' ]
    IAP_NOTAMS_ALL = IAP_NOTAMS_ALL[IAP_NOTAMS_ALL['IAP_AFFECTED'] == '1' ]
    IAP_NOTAMS_ALL = IAP_NOTAMS_ALL[IAP_NOTAMS_ALL['TEXT'].str.contains(" IAP ")]

    RNP_NOTAMS_ALL = DB_ACCESS[DB_ACCESS['TEXT'].str.contains("(RNP)")]
    RNP_NOTAMS_ALL = RNP_NOTAMS_ALL[RNP_NOTAMS_ALL['STATUS'] == 'ACTIVE' ]

    SPECI_NOTAMS_ALL = DB_ACCESS[DB_ACCESS['TEXT'].str.contains("SPECI")]
    SPECI_NOTAMS_ALL = SPECI_NOTAMS_ALL[SPECI_NOTAMS_ALL['STATUS'] == 'ACTIVE' ]

    AD_NOTAMS_ALL = DB_ACCESS[DB_ACCESS['TEXT'].str.contains(" AD ")]
    AD_NOTAMS_ALL = AD_NOTAMS_ALL[AD_NOTAMS_ALL['STATUS'] == 'ACTIVE' ]

    DB_ACCESS = DB_ACCESS.reset_index(drop=True)
    IAP_NOTAMS_ALL = IAP_NOTAMS_ALL.reset_index(drop=True)
    RNP_NOTAMS_ALL = RNP_NOTAMS_ALL.reset_index(drop=True)
    SPECI_NOTAMS_ALL = SPECI_NOTAMS_ALL.reset_index(drop=True)

    fileName = datetime.utcnow().strftime('%d%b%Y %H%MZ').upper() + ' 5GNOTAMS.xlsx'

    # Create a Pandas Excel writer using XlsxWriter as the engine.
    writer = pd.ExcelWriter('TEMP\\' + fileName)

    # Write each dataframe to a different worksheet.
    DB_ACCESS.to_excel(writer, sheet_name='5G NOTAMS ALL', index=False)
    AD_NOTAMS_ALL.to_excel(writer, sheet_name='ACTIVE AD NOTAMS', index=False)
    IAP_NOTAMS_ALL.to_excel(writer, sheet_name='ACTIVE IAP NOTAMS', index=False)
    RNP_NOTAMS_ALL.to_excel(writer, sheet_name='ACTIVE (RNP) NOTAMS', index=False)
    SPECI_NOTAMS_ALL.to_excel(writer, sheet_name='ACTIVE SPECI NOTAMS', index=False)
    ICAO_LIST.to_excel(writer, sheet_name='ICAOs MONITORED', index=False)

    DB_ACCESS['TEXT'] = DB_ACCESS['TEXT'].str.replace('<br/>','\n')
    AD_NOTAMS_ALL['TEXT'] = AD_NOTAMS_ALL['TEXT'].str.replace('<br/>','\n')
    IAP_NOTAMS_ALL['TEXT'] = IAP_NOTAMS_ALL['TEXT'].str.replace('<br/>','\n')
    RNP_NOTAMS_ALL['TEXT'] = RNP_NOTAMS_ALL['TEXT'].str.replace('<br/>','\n')
    SPECI_NOTAMS_ALL['TEXT'] = SPECI_NOTAMS_ALL['TEXT'].str.replace('<br/>','\n')

    # Close the Pandas Excel writer and output the Excel file.
    writer.save()
    writer.close()
    
    return DB_ACCESS,AD_NOTAMS_ALL,IAP_NOTAMS_ALL,RNP_NOTAMS_ALL,SPECI_NOTAMS_ALL, fileName

#FUNCTION TO SEND EMAIL WHEN NO CHANGES ARE FOUND:
def SEND_MAIL_NOTHING(processed_datetime,ICAO_LIST,l):
    #Print email being sent message to user...
    print('\nSending EMAIL with NOTHING TO REPORT...')
    #Specify host and server
    host = "smtp.netjets.com"
    server = smtplib.SMTP(host)
    
    #Build email body, including subject, sender, receiver(s) and MSG in html format (see html documentation for further reference on this:)
    subject = '[NOTAM TOOL] AD NOTAMS - 5G C-BAND' + ' - GENERATED: ' + processed_datetime 
    sender_email = "no-reply@netjets.com"
    recipients = ['kvonvaltier@netjets.com','cabe@netjets.com','ttaylor2@netjets.com','rbuergel@netjets.com', 'afaisca@ntasa.pt','pfonseca@ntasa.pt']
    recipients = ['afaisca@ntasa.pt']
    
    message = MIMEMultipart()
    message["From"] = sender_email
    message["To"] = ", ".join(recipients)
    message["Subject"] = subject
    
    # Add body to email (html MSG) - pre-message:
    MSG = """
    <p><b>ICAO CODES BEING MONITORED:</b><br/> (!REFER TO THE ATTACHED EXCEL FILE!) </p>
    
    <p><b>CRITERIA:</b><br/>Any NOTAM where any of the following keywords are present on the NOTAM text: 5G, C-BAND, 5g, c-band.</p>
    
    <p><b>SCHEDULE:</b><br/>This report will run daily at 1200Z (0700CMH). Version 2.0 SQLITE DataBase.</p>
    
    """ 
    #Fill the {} with python variables, to format text. 
    MSG_pre = MSG.format(ICAO_LIST=ICAO_LIST)
    
    # Add body to email
    MSG = """
    <h1>NOTHING TO REPORT...</h1>
    
    <p>This message is sent from Python for Lis-OpsTechnical.</p>
    
    """
    
    MSG = MSG_pre + MSG
    #Attach message (MIMEText) as html format. Can be plain as well if required.
    message.attach(MIMEText(MSG, "html"))
    
    DB_ACCESS,AD_NOTAMS_ALL,IAP_NOTAMS_ALL,RNP_NOTAMS_ALL,SPECI_NOTAMS_ALL, fileName = PREPARE_EXCEL(database_path,l)
    # Open PDF file in binary mode
    
    # We assume that the file is in the directory where you run your Python script from
    with open('TEMP\\' + fileName, "rb") as attachment:
        # The content type "application/octet-stream" means that a MIME attachment is a binary file
        part = MIMEBase("application", "octet-stream")
        part.set_payload(attachment.read())
    
    # Encode to base64
    encoders.encode_base64(part)
    
    # Add header 
    part.add_header(
        "Content-Disposition",
        f"attachment; filename= {fileName}",
    )
    
    # Add attachment to your message and convert it to string
    message.attach(part)
    
    text = message.as_string()
    
    try:
        server.sendmail(sender_email, recipients, text)
    except:
        print('Email TOO BIG!')
    
    #Quit server and show email successfuly sent message to user
    server.quit()
    
    print('Email Successfuly Sent to NJE!')         
    

#FUNCTION TO SEND EMAIL WHEN CHANGES ARE FOUND:
def SEND_MAIL_CHANGES(processed_datetime,ICAO_LIST,NOTAMS_DELETED,NOTAMS_NEW,NOTAMS_CHANGED,l):
    #Display message to user stating that email is being processed:
    print('\nSending EMAIL with NOTAM CHANGES...')

    DB_ACCESS,AD_NOTAMS_ALL,IAP_NOTAMS_ALL,RNP_NOTAMS_ALL,SPECI_NOTAMS_ALL, fileName = PREPARE_EXCEL(database_path,l)

    host = "smtp.netjets.com"
    server = smtplib.SMTP(host)
    
    subject = '[NOTAM TOOL] AD NOTAMS - 5G C-BAND' + ' - GENERATED: ' + processed_datetime 
    sender_email = "no-reply@netjets.com"
    recipients = ['kvonvaltier@netjets.com','cabe@netjets.com','ttaylor2@netjets.com','rbuergel@netjets.com', 'afaisca@ntasa.pt','pfonseca@ntasa.pt']
    recipients = ['afaisca@ntasa.pt']
    
    message = MIMEMultipart()
    message["From"] = sender_email
    message["To"] = ", ".join(recipients)
    message["Subject"] = subject

    NBR_EXEMPT_AIRPORTS_AMOC = str(len(DB_ACCESS[DB_ACCESS['EXEMPT_BY_AMOC'] == 1]))

    ####
    NBR_NEW = len(NOTAMS_NEW)
    NBR_AD_NEW = len(NOTAMS_NEW[NOTAMS_NEW['TEXT'].str.contains(" AD ")])
    NBR_IAP_NEW = len(NOTAMS_NEW[NOTAMS_NEW['TEXT'].str.contains(" IAP ")])
    NBR_RNP_NEW = len(NOTAMS_NEW[NOTAMS_NEW['TEXT'].str.contains("(RNP)")])
    NBR_SPECI_NEW = len(NOTAMS_NEW[NOTAMS_NEW['TEXT'].str.contains("SPECI")])

    NBR_DEL = len(NOTAMS_DELETED)
    NBR_AD_DEL = len(NOTAMS_DELETED[NOTAMS_DELETED['TEXT'].str.contains(" AD ")])
    NBR_IAP_DEL = len(NOTAMS_DELETED[NOTAMS_DELETED['TEXT'].str.contains(" IAP ")])
    NBR_RNP_DEL = len(NOTAMS_DELETED[NOTAMS_DELETED['TEXT'].str.contains("(RNP)")])
    NBR_SPECI_DEL = len(NOTAMS_DELETED[NOTAMS_DELETED['TEXT'].str.contains("SPECI")])

    # Add body to email (html MSG) - pre-message:
    MSG = """
    <p><b>ICAO CODES BEING MONITORED:</b><br/> (!REFER TO THE ATTACHED EXCEL FILE!) </p>
    
    <p><b>CRITERIA:</b><br/>Any NOTAM where any of the following keywords are present on the NOTAM text: 5G, C-BAND, 5g, c-band.</p>
    
    <p><b>SCHEDULE:</b><br/>This report will run daily 1200Z (0700 CMH). Version 2.0 SQLITE DataBase.</p>

    <p><b>THIS REPORT CONTAINS:</b></p>
    <b><span style="background-color: green; color: white;">{NBR_NEW} NEW 5G NOTAMS.{NBR_AD_NEW} RELATED TO "AD", {NBR_IAP_NEW} RELATED TO IAP, {NBR_RNP_NEW} RELATED TO (RNP), and {NBR_SPECI_NEW} RELATED TO SPECI (Special) PROCEDURES.</span></b></br>
    <b><span style="background-color: red; color: white;">{NBR_DEL} DELETED 5G NOTAMS.{NBR_AD_DEL} RELATED TO "AD", {NBR_IAP_DEL} RELATED TO IAP, {NBR_RNP_DEL} RELATED TO (RNP), and {NBR_SPECI_DEL} RELATED TO SPECI (Special) PROCEDURES.</span></b>

    <br></br><b>STATISTICS:</b></br>
    ACTIVE 5G NOTAMS (TOTAL): {ACTIVE_5G}</br>
    **ACTIVE 5G NOTAMS EXEMPTED BY AMOC**: {ACTIVE_SPECI}</br>
    ACTIVE 5G "AD" NOTAMS: {ACTIVE_AD}</br>
    ACTIVE 5G IAP NOTAMS: {ACTIVE_IAP}</br>
    ACTIVE 5G RNP NOTAMS: {ACTIVE_RNP}</br>
    ACTIVE 5G SPECI NOTAMS: {ACTIVE_SPECI}</br>
    """ 
    #Fill the {} with python variables, to format text. 
    MSG = MSG.format(NBR_AD_NEW=NBR_AD_NEW,NBR_AD_DEL=NBR_AD_DEL,ACTIVE_AD=str(len(AD_NOTAMS_ALL)),ICAO_LIST=ICAO_LIST,ACTIVE_5G=str(len(DB_ACCESS)),ACTIVE_IAP=str(len(IAP_NOTAMS_ALL)),ACTIVE_RNP=str(len(RNP_NOTAMS_ALL)),ACTIVE_SPECI=str(len(SPECI_NOTAMS_ALL)),NBR_NEW=NBR_NEW,NBR_IAP_NEW=NBR_IAP_NEW,NBR_RNP_NEW=NBR_RNP_NEW,NBR_SPECI_NEW=NBR_SPECI_NEW,NBR_DEL=NBR_DEL,NBR_IAP_DEL=NBR_IAP_DEL,NBR_RNP_DEL=NBR_RNP_DEL,NBR_SPECI_DEL=NBR_SPECI_DEL)
    
    #::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    #CANCELLED OR EXPIRED NOTAMS
    
    #Build "CANCELLED NOTAMS" header message and append to MSG
    MSG_line_deleted = """
    
    <span style="color:red;"><h1 style="background-color: red; color: white;">CANCELLED or EXPIRED 5G INTERFERENCE NOTAMS:</h1>
    """
    
    #Add the "CANCELLED NOTAMS" header message ONLY if the NOTAMS_DELETED dataframe is not empty:
    if len(NOTAMS_DELETED) != 0:       
        MSG = MSG + MSG_line_deleted 
    
    #If the dataframe is not empty, run through each and every line and build the text from it, including the ICAO, NOTAM nbr, QCODE (DECODED), FROM, TO and TEXT.
    for index, each in NOTAMS_DELETED.iterrows():
        if "(RNP)" in NOTAMS_DELETED['TEXT'][index]:
            MSG_line = """
            <b><span style="background-color: green; color: white;"> *** (RNP) PROCEDURE AFFECTED *** </span></b><br/>
            <b> ICAO:</b>  {ICAO}  <br/>
            <b> EXEMPTED BY AMOC:</b>  {EXEMPT}  <br/>
            <b><span style="background-color: red; color: white;"> NOTAM: {NOTAM} ({LONG}) </span></b><br/>
            <b> QCODE:</b>  {QCODE}  <br/>
            <b> FROM:</b>  {FROM}  <br/>
            <b> TO:</b>  {TO}  <br/>
            <b> TEXT:</b>  {TEXT}  <br/>
            """
        
        else:
            MSG_line = """
            <b> ICAO:</b>  {ICAO}  <br/>
            <b> EXEMPTED BY AMOC:</b>  {EXEMPT}  <br/>
            <b><span style="background-color: red; color: white;"> NOTAM: {NOTAM} ({LONG}) </span></b><br/>
            <b> QCODE:</b>  {QCODE}  <br/>
            <b> FROM:</b>  {FROM}  <br/>
            <b> TO:</b>  {TO}  <br/>
            <b> TEXT:</b>  {TEXT}  <br/>
            """
        
        #Add each line to the message, formatting the variables {} with values for specific row index and columns from dataframe "NOTAMS_DELETED".
        MSG_line = MSG_line.format(EXEMPT=str(NOTAMS_DELETED['EXEMPT_BY_AMOC'][index]).replace('1',' <b>YES</b>').replace('0',' NO'),ICAO=NOTAMS_DELETED['ICAO'][index][:4],NOTAM=NOTAMS_DELETED['NOTAM'][index],LONG=NOTAMS_DELETED['NOTAM_LONG_NUMBER'][index],QCODE=NOTAMS_DELETED['QCODE'][index],FROM=NOTAMS_DELETED['FROM'][index],TO=NOTAMS_DELETED['TO'][index],TEXT=NOTAMS_DELETED['TEXT'][index])
        MSG = MSG + MSG_line        
        #Append a spacer (line break) between notam entries:
        MSG = MSG + "<br/>"
    
    #::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    #NEW NOTAMS FOUND
    
    MSG_line_new = """
    </span><p><h1 style="background-color: black; color: white;">NEW 5G INTERFERENCE NOTAMS:</h1>

    """ 
    if len(NOTAMS_NEW):
        MSG = MSG + MSG_line_new
    
    for index, each in NOTAMS_NEW.iterrows():
        if "(RNP)" in NOTAMS_NEW['TEXT'][index]:
            MSG_line = """
            <b><span style="background-color: green; color: white;"> *** (RNP) PROCEDURE AFFECTED *** </span></b><br/>
            <b> ICAO:</b>  {ICAO}  <br/>
            <b> EXEMPTED BY AMOC:</b>  {EXEMPT}  <br/>
            <b><span style="background-color: black; color: white;"> NOTAM: {NOTAM} ({LONG}) </span></b><br/>
            <b> QCODE:</b>  {QCODE}  <br/>
            <b> FROM:</b>  {FROM}  <br/>
            <b> TO:</b>  {TO}  <br/>
            <b> TEXT:</b>  {TEXT}  <br/>
            """
        
        else:
            MSG_line = """
            <b> ICAO:</b>  {ICAO}  <br/>
            <b> EXEMPTED BY AMOC:</b>  {EXEMPT}  <br/>
            <b><span style="background-color: black; color: white;"> NOTAM: {NOTAM} ({LONG}) </span></b><br/>
            <b> QCODE:</b>  {QCODE}  <br/>
            <b> FROM:</b>  {FROM}  <br/>
            <b> TO:</b>  {TO}  <br/>
            <b> TEXT:</b>  {TEXT}  <br/>
            """
        
        MSG_line = MSG_line.format(EXEMPT=str(NOTAMS_NEW['EXEMPT_BY_AMOC'][index]).replace('1',' <b>YES</b>').replace('0',' NO'),ICAO=NOTAMS_NEW['ICAO'][index][:4],NOTAM=NOTAMS_NEW['NOTAM'][index],LONG=NOTAMS_NEW['NOTAM_LONG_NUMBER'][index],QCODE=NOTAMS_NEW['QCODE'][index],FROM=NOTAMS_NEW['FROM'][index],TO=NOTAMS_NEW['TO'][index],TEXT=NOTAMS_NEW['TEXT'][index])
        MSG = MSG + MSG_line
        MSG = MSG + "<br/>"
        
    #::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
    #CHANGED NOTAMS FOUND
    
    MSG_line_new = """
    </span><p><h1 style="background-color: orange; color: white;">AMENDED 5G INTERFERENCE NOTAMS:</h1>

    """ 
    if len(NOTAMS_CHANGED):
        MSG = MSG + MSG_line_new
    
    for index, each in NOTAMS_CHANGED.iterrows():
        MSG_line = """
        <b> ICAO:</b>  {ICAO}  <br/>
        <b><span style="background-color: orange; color: white;"> NOTAM: {NOTAM} ({LONG}) </span></b><br/>
        <b> QCODE:</b>  {QCODE}  <br/>
        <b> FROM:</b>  {FROM}  <br/>
        <b> TO:</b>  {TO}  <br/>
        <b> TEXT:</b>  {TEXT}  <br/>
        """
        
        MSG_line = MSG_line.format(ICAO=NOTAMS_CHANGED['ICAO'][index][:4],NOTAM=NOTAMS_CHANGED['NOTAM'][index],LONG=NOTAMS_CHANGED['NOTAM_LONG_NUMBER'][index],QCODE=NOTAMS_CHANGED['QCODE'][index],FROM=NOTAMS_CHANGED['FROM'][index],TO=NOTAMS_CHANGED['TO'][index],TEXT=NOTAMS_CHANGED['TEXT'][index])
        MSG = MSG + MSG_line
        MSG = MSG + "<br/>"
    
    message.attach(MIMEText(MSG, "html"))
    

    # Open PDF file in binary mode
    # We assume that the file is in the directory where you run your Python script from
    with open('TEMP\\' + fileName, "rb") as attachment:
        # The content type "application/octet-stream" means that a MIME attachment is a binary file
        part = MIMEBase("application", "octet-stream")
        part.set_payload(attachment.read())
    
    # Encode to base64
    encoders.encode_base64(part)
    
    # Add header 
    part.add_header(
        "Content-Disposition",
        f"attachment; filename= {fileName}",
    )
    
    # Add attachment to your message and convert it to string
    message.attach(part)
    
    text = message.as_string()
    
    try:
        server.sendmail(sender_email, recipients, text)
    except:
        print('Email TOO BIG')

    server.quit()
    print('Email Successfuly Sent to NJE!')         

def read_DB(database_path):
    try:
        # Create a SQL connection to our SQLite database
        con = sqlite3.connect(database_path)
        cursor = con.cursor()
        
        DB_ACCESS = pd.read_sql_query("SELECT * from NOTAMS", con)
        NEXT_ROW = len(DB_ACCESS)
        
        try:
            ID = cursor.lastrowid
        except:
            ID = 0
            pass
        con.commit()
        #print("Python Variables inserted successfully into SQLite table")
    
        cursor.close()
    
    except sqlite3.Error as error:
        print("Failed to insert Python variable into sqlite table", error)
    finally:
       if con:
           con.close()
       return ID, DB_ACCESS
   
def write_to_DB(sqlite_insert_with_param,data_tuple):
    try:
        # Create a SQL connection to our SQLite database
        con = sqlite3.connect(database_path)
        cursor = con.cursor()
       
        #addColumn = "ALTER TABLE USER_ACCESS ADD COLUMN LEASE_NBR integer"
        #cursor.execute(addColumn)
        
        cursor.execute(sqlite_insert_with_param, data_tuple)
        
        con.commit()
        #print("Python Variables inserted successfully into SQLite table")
    
        cursor.close()
    
    except sqlite3.Error as error:
        print("Failed to insert Python variable into sqlite table", error)
    finally:
       if con:
           con.close()
           
def QUERY_DB(sql_query_param,data_tuple):
    try:
        # Create a SQL connection to our SQLite database
        con = sqlite3.connect(database_path)
        cursor = con.cursor()
       
        #addColumn = "ALTER TABLE USER_ACCESS ADD COLUMN LEASE_NBR integer"
        #cursor.execute(addColumn)
        
        if data_tuple != []:
            cursor.execute(sql_query_param, data_tuple)
        else:
            cursor.execute(sql_query_param)
        response = cursor.fetchall()
        
        con.commit()
        #print("Python Variables inserted successfully into SQLite table")
    
        cursor.close()
    
    except sqlite3.Error as error:
        print("Failed to ffffff table", error)
    finally:
       if con:
           con.close()
           
       return response
           
           

database_path = 'Databases\\5G_NOTAMS.db'

try:
    AMOC =  pd.read_excel('Support Documents\\AMOC.xlsx')
    ICAOS_IN_AMOC = AMOC['ICAO Identifier'].to_list()
    print('Currently there are ' + str(len(ICAOS_IN_AMOC)) + ' ICAOs in AMOC (Exempt from NOTAM restrictions)...')
except:
    AMOC = []

df = pd.read_csv('Support Documents\\ICAOs Starting With K.csv')
df = df[df['ProceduresExist'] == 'Y']
df = df[df['RunwaysExist'] == 'Y']
l = df['Ident'].tolist()
n=50
ICAO_LIST = [l[i:i + n] for i in range(0, len(l), n)] 

ID, DB_ACCESS = read_DB(database_path)

#FETCH ALL 5G NOTAMS
NOTAMS_5G_ALL = []
for index, each in enumerate(ICAO_LIST):
    ICAO_LIST = each
    TEXT_INFO = 'NOTAMs'
    ####
    fresh_NOTAMS, ERRORS = text_info_NOTAM_NETJETS_API(TEXT_INFO, ICAO_LIST)
    ####
    NOTAMS_5G_ALL.append(fresh_NOTAMS)
    #peprint(ERRORS)

################################################################################################
# def run(ICAO_LIST):
#     fresh_NOTAMS, ERRORS = text_info_NOTAM_NETJETS_API('NOTAMs', ICAO_LIST)
#     NOTAMS_5G_ALL.append(fresh_NOTAMS)
#     print(len(NOTAMS_5G_ALL))


# import concurrent.futures
# with concurrent.futures.ThreadPoolExecutor() as executor:
#     executor.map(run, ICAO_LIST)
################################################################################################

#SET TRIGGER KEYWORD LIST TO FILTER NOTAMS
trigger_keyword_list = ['5G','c-band','5g','C-BAND']
#CONCAT LIST OF DF's to one single DF
NOTAMS_5G_ALL = pd.concat(NOTAMS_5G_ALL)
#FILTER rows by keywords contained in TEXT column
NOTAMS_5G_ALL = NOTAMS_5G_ALL[NOTAMS_5G_ALL['TEXT'].str.contains('|'.join(trigger_keyword_list))]

#Create a list of all currently active 5G NOTAMS from the downloaded dataframe from NOTAM service API
ACTIVE_NOTAMS_5G_LIST = NOTAMS_5G_ALL['NOTAM_LONG_NUMBER'].to_list()
# Query the database to find NOTAMS currently set to active where the NOTAM_LONG_NUMBER could not be found. This 
sql_query_param = """SELECT * FROM NOTAMS WHERE NOTAM_LONG_NUMBER NOT IN """ + str(ACTIVE_NOTAMS_5G_LIST).replace("[","(").replace("]",")")
response = QUERY_DB(sql_query_param,[])
NOTAMS_DELETED = pd.DataFrame(response, columns=["ID", "FIR","ICAO","NOTAM","QCODE","QCODES_UNDECODED","FROM","TO","SCHEDULE","TEXT","ITEMF","ITEMG","NOTAM_LONG_NUMBER","VERSION","SOURCE","NOTAM_TYPE","RUNWAYS","STATUS","ADDED_ON","DELETED_ON",'IAP_AFFECTED','EXEMPT_BY_NOTAM'])
NOTAMS_DELETED = NOTAMS_DELETED[NOTAMS_DELETED['STATUS'] == 'ACTIVE']

for index, each in NOTAMS_DELETED.iterrows():
    sqlite_insert_with_param = """UPDATE NOTAMS SET
                          STATUS=?,DELETED_ON=?
                          WHERE ID=? """
    data_tuple = ('DELETED',datetime.utcnow().strftime('%d%b%Y %H%MZ').upper(),each['ID']) 
    write_to_DB(sqlite_insert_with_param,data_tuple)    

for index, each in NOTAMS_5G_ALL.iterrows():
    sql_query_param = """SELECT ID,"TEXT" FROM NOTAMS WHERE ICAO=? AND NOTAM_LONG_NUMBER = ? AND STATUS = ?"""
    data_tuple = (each['ICAO'],each['NOTAM_LONG_NUMBER'],'ACTIVE')
    
    response = QUERY_DB(sql_query_param,data_tuple)
    
    if each['QCODES_UNDECODED'] != "CALS" or (" IAP " in each['TEXT']) or (" SPECIAL " in each['TEXT']):
        IAP_AFFECTED = True
    else:
        IAP_AFFECTED = False

    if each['ICAO'] in ICAOS_IN_AMOC:
        EXEMPT_BY_AMOC = 1
        each['EXEMPT_BY_AMOC'] = 1
    else:
        EXEMPT_BY_AMOC = 0
        each['EXEMPT_BY_AMOC'] = 0

    if response == []:
        print('NEW NOTAM! ADDING TO DB...')

        sqlite_insert_with_param = """INSERT INTO NOTAMS
                              (FIR,ICAO,NOTAM,QCODE,QCODES_UNDECODED,FR,TIL,SCHEDULE,TEXT,ITEMF,ITEMG,NOTAM_LONG_NUMBER,VERSION,SOURCE,NOTAM_TYPE,RUNWAYS,STATUS,ADDED_ON,IAP_AFFECTED,EXEMPT_BY_AMOC)
                                  VALUES 
                              (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) """                                         
        data_tuple = (each['FIR'],each['ICAO'],each['NOTAM'],each['QCODE'],each['QCODES_UNDECODED'],each['FROM'],each['TO'],each['SCHEDULE'],each['TEXT'],each['ITEMF'],each['ITEMG'],each['NOTAM_LONG_NUMBER'],each['VERSION'],each['SOURCE'],each['NOTAM_TYPE'],each['RUNWAYS'],'ACTIVE',datetime.utcnow().strftime('%d%b%Y %H%MZ').upper(),IAP_AFFECTED,EXEMPT_BY_AMOC)
        write_to_DB(sqlite_insert_with_param,data_tuple)
        
        NOTAMS_NEW.append(each.to_frame().T)
        
    elif response[0][1] != each['TEXT']:
        print('NOTAM TEXT CHANGED. UPDATING RECORD.........')
        
        sqlite_insert_with_param = """UPDATE NOTAMS SET
                              FIR=?,ICAO=?,NOTAM=?,QCODE=?,QCODES_UNDECODED=?,FR=?,TIL=?,SCHEDULE=?,TEXT=?,ITEMF=?,ITEMG=?,NOTAM_LONG_NUMBER=?,VERSION=?,SOURCE=?,NOTAM_TYPE=?,RUNWAYS=?,STATUS=?,IAP_AFFECTED=?,EXEMPT_BY_AMOC=?
                              WHERE ID=? """
        data_tuple = (each['FIR'],each['ICAO'],each['NOTAM'],each['QCODE'],each['QCODES_UNDECODED'],each['FROM'],each['TO'],each['SCHEDULE'],each['TEXT'],each['ITEMF'],each['ITEMG'],each['NOTAM_LONG_NUMBER'],each['VERSION'],each['SOURCE'],each['NOTAM_TYPE'],each['RUNWAYS'],'ACTIVE',IAP_AFFECTED,EXEMPT_BY_AMOC,response[0][0]) 
        write_to_DB(sqlite_insert_with_param,data_tuple)
        
        NOTAMS_CHANGED.append(each.to_frame().T)
        
    elif response[0][1] == each['TEXT']:
        #print('NOTAM NOT CHANGED AND STILL ACTIVE')
        pass

try:
    NOTAMS_NEW = pd.concat(NOTAMS_NEW)
    NOTAMS_NEW = NOTAMS_NEW.reset_index(drop=True)
except:
    pass
try:
    NOTAMS_CHANGED = pd.concat(NOTAMS_CHANGED)
    NOTAMS_CHANGED = NOTAMS_CHANGED.reset_index(drop=True)
except:
    pass

if len(NOTAMS_DELETED) != 0 or len(NOTAMS_NEW) != 0 or len(NOTAMS_CHANGED) != 0:
    #GET CURRENT UTC TIME to print out email messages:
    current_UTC = datetime.utcnow()
    current_UTC = current_UTC.strftime("%d%b%Y-%H%M")
    processed_datetime = current_UTC
    processed_datetime = (current_UTC + "Z").upper()
    try:
        if NOTAMS_NEW == []:
            NOTAMS_NEW = pd.DataFrame()
    except:
        pass
    try:
        if NOTAMS_CHANGED == []:
            NOTAMS_CHANGED = pd.DataFrame()
    except:
        pass

        
    SEND_MAIL_CHANGES(processed_datetime,l,NOTAMS_DELETED,NOTAMS_NEW,NOTAMS_CHANGED,l)
    
else:
    print('NO CHANGES FOUND WHEN COMPARING NEW NOTAMS WITH DATABASE...')
    current_UTC = datetime.utcnow()
    current_UTC = current_UTC.strftime("%d%b%Y-%H%M")
    processed_datetime = current_UTC
    processed_datetime = (current_UTC + "Z").upper()
    SEND_MAIL_NOTHING(processed_datetime,ICAO_LIST,l)
    print('Email with no changes sent!')
        